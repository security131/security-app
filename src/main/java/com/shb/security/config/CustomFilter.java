package com.shb.security.config;


import com.shb.security.utils.CustomerUserDetail;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String userName = "tupv";
        String password = "123456aA@";
//         Authenticate user (example assumes custom UserDetails implementation);

//         UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
        CustomerUserDetail customerUserDetail = new CustomerUserDetail(userName, password);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                customerUserDetail, password, customerUserDetail.getAuthorities());
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

//         Set authentication in the SecurityContext
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

//         Continue with the request handling
        filterChain.doFilter(request, response);
    }

}
