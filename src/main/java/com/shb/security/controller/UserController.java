package com.shb.security.controller;

import com.shb.security.dto.UserDTO;
import com.shb.security.model.Role;
import com.shb.security.model.User;
import com.shb.security.service.RoleService;
import com.shb.security.service.UserService;
import com.shb.security.utils.MultiThreading;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Autowired
    private MultiThreading multiThreading;

    @GetMapping("/getUserById")
    public ResponseEntity<?> getUser(){
//        User user = userService.getUser(1);
//        return ResponseEntity.ok().body(user);
//        multiThreading.async();
        return  null;
    }
    @GetMapping("/getUserByUserName")
    public ResponseEntity<?> getUserByUserName(){
        User user = userService.getUserByUserName("luongpt");
        return ResponseEntity.ok().body(user);
    }
    @PostMapping("/getUser")
    public ResponseEntity<?> getUserTestPost(@RequestBody UserDTO userDTO){
        User user = userService.getUserByUserName(userDTO.getUserName());
        return ResponseEntity.ok().body(user);
    }

}
