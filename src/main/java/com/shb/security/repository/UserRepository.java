package com.shb.security.repository;

import com.shb.security.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    User getById(Integer id);

    User getByUserName(String userName);
}
