package com.shb.security.service.impl;

import com.shb.security.model.Role;
import com.shb.security.repository.RoleDao;
import com.shb.security.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public Role getRoleById(Integer id) {
        return roleDao.findById(id).orElse(null);
    }
}
