package com.shb.security.service.impl;

import com.shb.security.model.User;
import com.shb.security.repository.UserRepository;
import com.shb.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Override
    public User getUser(Integer id) {
//        System.out.println(id);
//        Long start = System.currentTimeMillis();
//        System.out.println(start);
        try {
            Thread.sleep(2000l);
            System.out.println("sleep 200");
        }catch (Exception e){}
        User user =  userRepository.getByUserName("tupv");
//        Long end = System.currentTimeMillis();
//        System.out.println("length : " + (end - start));
//        System.out.println("start get by id : " + start);
//        System.out.println("end get by id : " + end);

        return null;
    }

    @Override
    public User getUserByUserName(String userName) {
//        System.out.println("start get by name : " + System.currentTimeMillis());
        User user =  userRepository.getByUserName(userName);
//        System.out.println(user.getUserName());
        return user;
    }
}
