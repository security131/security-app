package com.shb.security.service;

import com.shb.security.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    User getUser(Integer id);

    User getUserByUserName(String userName);
}
