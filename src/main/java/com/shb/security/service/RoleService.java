package com.shb.security.service;

import com.shb.security.model.Role;

public interface RoleService {

    public Role getRoleById(Integer id);
}
